package com.example.ezlive;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.AuthResult;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class registro extends AppCompatActivity {

    EditText txt_nombre;
    EditText txt_apellido;
    TextView txt_profesion;
    Spinner spn;
    EditText ciudad;
    EditText telefono;
    EditText correo;
    EditText txt_contraseña_1;
    EditText txt_confirmar_pwd;
    TextView txt_contraseña;
    TextView txt_confirmar;
    Button crear_cuenta;

    //firebase
    //private FirebaseAuth mAuth;


    //FUENTE

    Typeface dealer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        // Initialize Firebase Auth
        //mAuth = FirebaseAuth.getInstance();



        //SPINNER
        txt_profesion = (TextView)findViewById(R.id.profesion);
        spn = (Spinner)findViewById(R.id.spinner_1);


        txt_nombre = (EditText) findViewById(R.id.txt_nombre);
        txt_apellido = (EditText) findViewById(R.id.txt_apellido);
        ciudad = (EditText) findViewById(R.id.ciudad);
        telefono = (EditText) findViewById(R.id.telefono);
        correo = (EditText) findViewById(R.id.correo);
        txt_contraseña_1 = (EditText)findViewById(R.id.txt_contraseña_1);
        txt_confirmar_pwd = (EditText) findViewById(R.id.txt_confirmar_pwd);
        txt_contraseña = (TextView) findViewById(R.id.txt_contraseña);
        txt_confirmar = (TextView) findViewById(R.id.txt_confirmar);

        //BOTONES
        crear_cuenta = (Button)findViewById(R.id.crear_cuenta_2);


        //FUENTE DEALER

        String fuente_dealer = "fuentes/dealer.ttf";
        this.dealer = Typeface.createFromAsset(getAssets(), fuente_dealer);
        txt_nombre.setTypeface(dealer);
        txt_apellido.setTypeface(dealer);
        ciudad.setTypeface(dealer);
        telefono.setTypeface(dealer);
        correo.setTypeface(dealer);
        txt_contraseña.setTypeface(dealer);
        txt_confirmar.setTypeface(dealer);
        txt_contraseña_1.setTypeface(dealer);
        txt_confirmar_pwd.setTypeface(dealer);
        crear_cuenta.setTypeface(dealer);
        txt_profesion.setTypeface(dealer);


        //----SPINNER----

        //Creo un array de tipo string y le asigno el nombre profesiones
        ArrayList<String> profesiones = new ArrayList<String>();

        //utilizando add agrego los elementos que mostrará el spinner
        profesiones.add("Seleccionar profesión: ");
        profesiones.add("Informatica");
        profesiones.add("Electricidad");
        profesiones.add("Mecánica");
        profesiones.add("Jardinería");
        profesiones.add("Medicina");
        profesiones.add("Carpintería");
        profesiones.add("Construcciones");

        //para conectar el arreglo con el spinner creo el adaptador y le asigno el nombre adapter y le envío 3 elementos:
        //el contexto de la aplicacion o simplemente this
        //defino que sea un simple_spinner_item
        //el nombre del arreglo
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,profesiones);

        //asigno el adaptador que creé a la variable del spinner
        spn.setAdapter(adapter);


        //para guardar la seleccion del spinner se utiliza setOnItemSelectedListener
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //los parametros dentro del parentesis seran siempre los mismos
                txt_profesion.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        crear_cuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrarUsuario();

            }
        });


    }

    private void RegistrarUsuario(){
        //se obtienen los textos ingresados en usuario y contraseña
        String nom = txt_nombre.getText().toString().trim();
        String apell = txt_apellido.getText().toString().trim();
        String prof = txt_profesion.getText().toString().trim();
        String ciud = ciudad.getText().toString().trim();
        String tlf = telefono.getText().toString().trim();
        String email = correo.getText().toString().trim();
        String pwd = txt_contraseña_1.getText().toString().trim();
        String conf = txt_confirmar_pwd.getText().toString().trim();


        //se verifica que no esten vacíos

        if(TextUtils.isEmpty(nom)){
            Toast.makeText(getApplicationContext(), "Agrega tu nombre",Toast.LENGTH_SHORT).show();
            return;
        }else {
            if(TextUtils.isEmpty(apell)){
                Toast.makeText(getApplicationContext(), "Agrega tu apellido",Toast.LENGTH_SHORT).show();
                return;
            }else {
                if(TextUtils.isEmpty(prof)){
                    Toast.makeText(getApplicationContext(), "Escoge tu profesion",Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    if(TextUtils.isEmpty(ciud)){
                        Toast.makeText(getApplicationContext(), "Agrega tu ciudad",Toast.LENGTH_SHORT).show();
                        return;
                    }else {
                        if(TextUtils.isEmpty(tlf)){
                            Toast.makeText(getApplicationContext(), "Agrega tu teléfono",Toast.LENGTH_SHORT).show();
                            return;
                        }else {
                            if(TextUtils.isEmpty(email)){
                                Toast.makeText(getApplicationContext(), "el email es incorrecto",Toast.LENGTH_SHORT).show();
                                return;
                            }else{
                                if(TextUtils.isEmpty(pwd)){
                                    Toast.makeText(getApplicationContext(),"Ingresa una contraseña", Toast.LENGTH_SHORT).show();
                                    return;
                                }else{
                                    if(TextUtils.isEmpty(conf)){
                                        Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden",Toast.LENGTH_SHORT).show();
                                        return;
                                    }else{
                                        Intent crear = new Intent(registro.this, perfiles.class);
                                        Toast.makeText(getApplicationContext(), "Registrado", Toast.LENGTH_SHORT).show();
                                        startActivity(crear);
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }


















    }




}
