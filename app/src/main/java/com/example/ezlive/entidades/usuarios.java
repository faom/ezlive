package com.example.ezlive.entidades;

public class usuarios {

    private String nombre;
    private String apellido;
    private String profesion;
    private String ciudad;
    private String telefono;
    private String usuario;

    public usuarios(String nombre, String apellido, String profesion, String ciudad, String telefono, String usuario) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.profesion = profesion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.usuario = usuario;
    }
}
