package com.example.ezlive;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class splash extends AppCompatActivity {

    Typeface moonbright;

    //Constante de tipo entero que representa el tiempo en milisegundos que se muestra nuestra actividad al usuario
    static int TIMEOUT_MILLIS = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);






        //FUENTE MOONBRIGHT


        String i = "fuentes/moonbright.ttf";
        this.moonbright = Typeface.createFromAsset(getAssets(), i);
        //blue.setTypeface(moonbright);





        //QUITAMOS LA OPCION BAR DE LA PANTALLA SPLASH
        getSupportActionBar().hide();

        //LA CLASE Handler PERMITE ENVIAR OBJETOS DE TIPO Runnable.
        //UN OBJETO DE TIPO RUNNABLE DA SIGNIFICADO A UN HILO DE PROCESO
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //INTENT ES LA DEFINICION ABSTRACTA DE UNA OPERACION A REALIZAR .
                //PUEDE SER USADO PARA UNA ACTIVITY, broadcast receiver, servicios.
                Intent i = new Intent(splash.this, login.class);
                startActivity(i);

                //FINALIZAR ESTA ACTIVITY
                finish();
            }
        },TIMEOUT_MILLIS);
    }
}
