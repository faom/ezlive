package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.strictmode.IntentReceiverLeakedViolation;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class descripcion_1 extends AppCompatActivity {


    ImageView flecha;
    ImageView sms_1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_1);

        flecha = (ImageView)findViewById(R.id.flecha_1);
        sms_1 = (ImageView)findViewById(R.id.sms_1);

        flecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regresar = new Intent(descripcion_1.this, perfiles.class);
                startActivity(regresar);
            }
        });

        sms_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatear = new Intent(descripcion_1.this, chat.class);
                startActivity(chatear);
            }
        });




    }
}
