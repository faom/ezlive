package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;

public class descripcion_3 extends AppCompatActivity {

    ImageView flecha_3;
    ImageView sms_3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_3);

        flecha_3 = (ImageView)findViewById(R.id.flecha_3);
        sms_3 = (ImageView)findViewById(R.id.sms_3);


        flecha_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regresar_3 = new Intent(descripcion_3.this, perfiles.class);
                startActivity(regresar_3);
            }
        });

        sms_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chat_3 = new Intent(descripcion_3.this, chat.class);
                startActivity(chat_3);
            }
        });
    }
}
